package com.leyou.item.controller;

import com.leyou.item.pojo.SpecGroup;
import com.leyou.item.pojo.SpecParam;
import com.leyou.item.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author HongBo Wang
 * @data 2018/6/27 10:44
 */
@RestController
@RequestMapping("spec")
public class SpecificationController {

    @Autowired
    private SpecificationService specificationService;

    @GetMapping("/groups/{cid}")
    public ResponseEntity<List<SpecGroup>> querySpecGroupsByCid(@PathVariable("cid")Long cid){
        List<SpecGroup> specGroups = this.specificationService.querySpecGroupsByCid(cid);
        if(specGroups==null || specGroups.size()==0){
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return  ResponseEntity.ok(specGroups);
    }

    @GetMapping("/params")
    public ResponseEntity<List<SpecParam>> querySpecParamByGid(
            @RequestParam(value = "gid",required = false)Long gid,
             @RequestParam(value="cid", required = false) Long cid,
            @RequestParam(value="searching", required = false) Boolean searching,
            @RequestParam(value="generic", required = false) Boolean generic
    ){
        List<SpecParam> specParams = this.specificationService.querySpecParamsByGid(gid,cid,searching,generic);
        if(specParams==null || specParams.size()==0){
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return  ResponseEntity.ok(specParams);
    }

    @GetMapping("groups")
    public ResponseEntity<List<SpecGroup>> querySpecGroupAndParam(@RequestParam("cid") Long cid){
        List<SpecGroup> list = this.specificationService.querySpecGroupAndParam(cid);
        if(list == null || list.size() == 0){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(list);
    }

}
