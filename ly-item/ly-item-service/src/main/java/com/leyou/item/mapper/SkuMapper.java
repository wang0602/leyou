package com.leyou.item.mapper;

import com.leyou.item.pojo.Sku;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author HongBo Wang
 * @data 2018/6/28 20:20
 */
public interface SkuMapper extends Mapper<Sku> {
}
