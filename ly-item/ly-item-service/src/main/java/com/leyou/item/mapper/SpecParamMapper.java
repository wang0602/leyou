package com.leyou.item.mapper;

import com.leyou.item.pojo.SpecParam;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author HongBo Wang
 * @data 2018/6/27 11:21
 */
public interface SpecParamMapper extends Mapper<SpecParam> {

}
