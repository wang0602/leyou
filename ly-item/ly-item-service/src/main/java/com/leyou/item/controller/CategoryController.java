package com.leyou.item.controller;

import com.leyou.item.pojo.Category;
import com.leyou.item.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author HongBo Wang
 * @data 2018/6/23 21:30
 */
@RestController
@RequestMapping("category")
public class CategoryController {
    //注入service
    //调用service返回相应实体
    @Autowired
    private CategoryService categoryService;

    /**根据父id查商品分类信息
     * @param pid
     * @return
     */
    @GetMapping("list")
    public ResponseEntity<List<Category>> queryByParentId(
            @RequestParam(value = "pid",defaultValue = "0")Long pid){
        List<Category> list = this.categoryService.queryByParentId(pid);
        if (list==null || list.size() < 1){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(list);
    }

    /**根据商品分类id查询名称
     * @param ids
     * @return
     */
    @GetMapping("names")
    public ResponseEntity<List<String>> queryNameByIds(@RequestParam("ids") List<Long> ids){
        List<String> list = this.categoryService.queryNameByIds(ids);
        // 判断是否查询到
        if(list == null || list.size() <= 0 ){
            // 返回404
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        // 成功返回200
        return ResponseEntity.ok(list);
    }
}
