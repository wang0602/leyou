package com.leyou.cart.client;

import com.leyou.item.api.GoodsApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author HongBo Wang
 * @data 2018/7/22 10:58
 */
@FeignClient(value = "item-service")
public interface GoodsClient extends GoodsApi {

}
