package com.leyou.search.client;

import com.leyou.item.api.BrandApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author HongBo Wang
 * @data 2018/7/3 21:02
 */
@FeignClient("item-service")
public interface BrandClient extends BrandApi{

}
