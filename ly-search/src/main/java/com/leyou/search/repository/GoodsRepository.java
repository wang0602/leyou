package com.leyou.search.repository;

import com.leyou.search.pojo.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author HongBo Wang
 * @data 2018/7/1 17:43
 */
public interface GoodsRepository extends ElasticsearchRepository<Goods,Long> {
}
