package com.leyou.user.mapper;

import com.leyou.item.pojo.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author HongBo Wang
 * @data 2018/7/9 10:32
 */
public interface UserMapper extends Mapper<User> {
}
